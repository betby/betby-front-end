# Betby


## Lancement local
- Avoir le daemon docker fonctionnel
- Se positionner à la racine du projet
- ```docker build -t registry.gitlab.com/betby/betby-front-end/main:latest .```
- ```docker-compose up```
- Le site est accessible au "http://localhost:5000/"



## Auteurs
- Daniel Karl
- Rossignon Morgan
- Salomode Florian
