# Stage 1: Build the Angular app
FROM node:latest as build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build --prod

# Stage 2: Create a production-ready container
FROM nginx:1.17.1-alpine
COPY --from=build /app/dist/betby /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/conf.d/default.conf