import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { StandingsComponent } from './standings/standings.component';
import {RouterLink, RouterLinkActive, RouterOutlet} from "@angular/router";
import { MatchListComponent } from './match-list/match-list.component';
import { MatchComponent } from './match/match.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {ReactiveFormsModule} from "@angular/forms";
import { TeamComponent } from './team/team.component';
import {VariablesGlobales} from "./variablesGlobale";
import { ProfileComponent } from './profile/profile.component';
import { MatchListByTeamComponent } from "./match-list-by-team/match-list-by-team.component";
import { NotFoundComponent } from './not-found/not-found.component';
import {TokenInterceptor} from "./token.interceptor";
import { BetComponent } from './bet/bet.component';
import { BetListComponent } from './bet-list/bet-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LandingPageComponent,
    StandingsComponent,
    MatchListComponent,
    MatchComponent,
    LoginComponent,
    RegisterComponent,
    TeamComponent,
    ProfileComponent,
    MatchListByTeamComponent,
    NotFoundComponent,
    BetComponent,
    BetListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterLink,
    RouterLinkActive,
    RouterOutlet,
    ReactiveFormsModule
  ],
  providers: [VariablesGlobales,
    {provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi : true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
