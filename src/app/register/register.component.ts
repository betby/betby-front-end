import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  popUpSuccess : boolean = false;
  popUpFailure : boolean = false;

  errorMessage !:string;


  constructor(
    private formBuilder: FormBuilder,
    private registerService: AuthService,

    private _router : Router
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]],
     confirmPassword: ['', Validators.required]
   }, {
     validator: this.passwordMatchValidator
   });
  }

  passwordMatchValidator(form: FormGroup) {
    /*if(form.get('password')?.value === form.get('confirmPassword')?.value){
      return null;
    }
    else{
      this.errorMessage = "Les mots de passes ne concordent pas !";
      this.popUpFailure = true;
      return { 'mismatch': true};
    }*/
    return (form.get('password')?.value === form.get('confirmPassword')?.value)
      ? null : { 'mismatch': true };
  }

  onSubmit() {
    const username = this.registerForm.get('username')?.value;
    const password = this.registerForm.get('password')?.value;
    this.registerService.registerUser({username,password}).subscribe( {
      next: data =>{
        this.popUpSuccess = true;
      },
      error : error =>{
        this.errorMessage = error.error;
        this.popUpFailure = true;
      }
    });
  }
  onClickToConnect(){
    this._router.navigateByUrl('login').then();
  }
  onClickToRetry(){
    this.popUpFailure = false;
    this.registerForm.reset();
  }
}
