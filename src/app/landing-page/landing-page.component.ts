import { Component } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent {
  backgroundUrl: string = "../../assets/images/betbycards.png";
  backgroundAlt: string = "Impossible de charger l'image";
}

