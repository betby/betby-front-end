import { Component,OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit{
  money!: any;
  name!: any;
  constructor(private authService : AuthService) {
  }

  ngOnInit(): void {
    this.authService.getCurrencyAmount().subscribe(val =>{
      this.money = val;

    });
    this.name = localStorage.getItem('userName');
  }
  onClickToDelete(){
    this.authService.deleteUser();
    this.authService.logoutUser();
  }


}
