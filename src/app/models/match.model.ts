

export class Match {
  id!: number;
  homeTeamId!: number;
  visitorTeamId!: number;
  leagueId!: number;
  homeScore!: number;
  visitorScore!: number;
  isFinished!: boolean;
  startDate!: Date;
  nameLocal!: string;
  nameVisitor!: string;
  logoLocal!: string;
  logoVisitor!: string;
  homeOdd?: number;
  visitorOdd?: number;
  drawOdd?: number;
}
