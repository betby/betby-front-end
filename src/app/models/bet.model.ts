export class Bet {

  constructor( userid : number, moneybet : number, matchid : number, matchbettype : string){
    this.userid = userid;
    this.moneybet = moneybet;
    this.matchid = matchid;
    this.matchBetType = matchbettype;
  }
  id! : number;
  userid!: number;
  moneybet! : number;
  matchid! : number;
  matchBetType !: string;
}
