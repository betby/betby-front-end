import {Match} from "./match.model";

export class Team {
  constructor( id : number, name : string, logo : string, point : number, position :number, leagueId : number, matchs : Match[]) {
    this.id = id;
    this.name = name;
    this.logo = logo;
    this.point = point;
    this.position = position;
    this.leagueId = leagueId;
    this.matchs = matchs;
  }
  id!: number;
  name!: string;
  logo!: string;
  point!: number;
  position!: number;
  leagueId!: number;
  matchs!: Match[]
}
