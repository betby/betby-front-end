export class BetResult {
  id!: number;
  userId!: number;
  betAccepted !: boolean;
  betAmount !: number;
  potentialGain !: number;
  odd!: number;
  resultObtained !: boolean;
  isWon !: boolean;
  matchId!: number;
  matchBetType !: string;
}
