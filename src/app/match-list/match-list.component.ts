import {Component, OnInit} from '@angular/core';
import {Match} from "../models/match.model";
import {MatchService} from "../services/match.service";
import {TeamService} from "../services/team.service";
import {formatDate} from "@angular/common";


@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss']
})
export class MatchListComponent implements OnInit{

  matchs : Match[] =[];
  competition : string = "Top 14";
  choiceMatch : string = "Matchs à venir";
  idCompetition : number = 16;
  constructor(private matchservice : MatchService, private teamservice : TeamService){

  }

  ngOnInit(): void {
    this.matchservice.getFutureMatches(this.idCompetition).subscribe( value => { this.fillList(value) });
  }
  onclickCompetition( competition : string){
    this.competition = competition;
    if(competition === "Top 14"){
      this.idCompetition = 16;
    }else{
      this.idCompetition = 54;
    }
    this.refresh();
  }
  onclick( choice : string){
    this.choiceMatch = choice;
    this.refresh();
  }
  refresh(){
    this.matchs = [];
    if(this.choiceMatch === "Matchs finis"){
      this.matchservice.getPastMatches(this.idCompetition).subscribe( value => {
        value.sort( (a,b) => formatDate(a.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') < formatDate(b.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') ? 1 : -1);
        this.fillList(value);
      });
    }else if(this.choiceMatch === "Matchs du jour"){
      this.matchservice.getCurrentMatches(this.idCompetition).subscribe( value => {
        value.sort( (a,b) => formatDate(a.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') < formatDate(b.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') ? -1 : 1);
        this.fillList(value);
      });
    }else if(this.choiceMatch === "Matchs à venir"){
      this.matchservice.getFutureMatches(this.idCompetition).subscribe( value => {
        value.sort( (a,b) => formatDate(a.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') < formatDate(b.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') ? -1 : 1);
        this.fillList(value);
      });

    }
  }
  fillList( value : Match[]){
    value.forEach( val => {
      this.teamservice.getTeamById(val.homeTeamId, this.idCompetition).subscribe( t => {
        val.nameLocal = t.name;
        val.logoLocal = t.logo;
      });
      this.teamservice.getTeamById(val.visitorTeamId, this.idCompetition).subscribe( t => {
        val.nameVisitor = t.name;
        val.logoVisitor = t.logo;
      });
      this.matchs.push( val );
    });
  }




}
