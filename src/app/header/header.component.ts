import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {interval, Observable} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  logoPath!: string;
  coinPath!: string;
  interval$ !: Observable<number>;
  currencyMoney!: string | null;
  userName!: string | null;
  constructor(private _auth : AuthService) {
    this.logoPath = "../../assets/images/betbylogo.png";
    this.coinPath = "../../assets/images/coin.png";

  }
  onClick(){
    this._auth.logoutUser();

  }
  isConnected(): boolean{
    return this._auth.loggedIn();

  }

  ngOnInit(): void {
    this.interval$ = interval(3000);
    this.interval$.subscribe(val=>{
      if(this.isConnected()) {
        this.currencyMoney = localStorage.getItem("currencyAmount")
        this.userName = localStorage.getItem("userName");
      }
      });
    }
}
