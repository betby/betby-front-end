import {Component, Input, OnInit} from '@angular/core';
import {MatchService} from "../services/match.service";
import {BetResult} from "../models/bet-result.model";
import {TeamService} from "../services/team.service";

@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.scss']
})
export class BetComponent implements OnInit{

  @Input() bet! : BetResult;
  teamHomeName !: string;
  teamHomeLogoUrl !: string;
  teamVisitorName !: string;
  teamVisitorLogoUrl !: string;
  date!: Date;

  constructor(private matchService: MatchService, private teamService : TeamService) {
  }

  ngOnInit(): void {
    this.matchService.getMatch(this.bet.matchId).subscribe(value => {
      this.date = value.startDate;
      this.teamService.getTeamById(value.homeTeamId,value.leagueId).subscribe(team => {
        this.teamHomeName = team.name;
        this.teamHomeLogoUrl = team.logo;
      });
      this.teamService.getTeamById(value.visitorTeamId,value.leagueId).subscribe(team=>{
        this.teamVisitorName = team.name;
        this.teamVisitorLogoUrl = team.logo;
      });
    });
  }
  getTeamBet(){
    return (this.bet.matchBetType=== "Home")?this.teamHomeName : (this.bet.matchBetType=== "Visitor")? this.teamVisitorName : "Match null";
  }

}
