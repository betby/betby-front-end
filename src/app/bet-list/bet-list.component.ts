import { Component , OnInit} from '@angular/core';
import {BetService} from "../services/bet.service";
import {BetResult} from "../models/bet-result.model";


@Component({
  selector: 'app-bet-list',
  templateUrl: './bet-list.component.html',
  styleUrls: ['./bet-list.component.scss']
})
export class BetListComponent implements OnInit{
  bets: BetResult[] = [];
  betsToDisplay: BetResult[] = [];
  cpt : number = 0;

  userId : number = -1;
  display : string = "";

  constructor(private betService : BetService) {
  }
  ngOnInit(): void {
    const userid = localStorage.getItem('id');
    if(userid){
      this.userId = Number.parseInt(userid);
      this.betService.getAllBetsByUserId(this.userId).subscribe( values =>{
        values.forEach(value=>{
          this.bets.push(value);
          this.cpt++;
        });
      });
    }
    this.betsToDisplay = this.bets;
  }
  onClickChoiceBet(choice : string){
    this.display = choice;
    if(choice === "En cours"){
      this.betsToDisplay = this.bets.filter(bet=>{
        return !bet.resultObtained;
      });

    }
    else{
      this.betsToDisplay = this.bets.filter(bet=>{
        return bet.resultObtained;
      });

    }
    this.cpt = this.betsToDisplay.length;
  }

}
