import {Component, OnInit} from '@angular/core';
import {Team} from "../models/team.model";
import {LeagueService} from "../services/league.service";
import {Match} from "../models/match.model";

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.scss']
})
export class StandingsComponent implements OnInit{

  name! : string;

  logo! : string;
  teams : Team[] = [];

  constructor( private leagueservice : LeagueService){

  }
  ngOnInit(): void {
    this.leagueservice.getLeagueById( 16 ).subscribe( value => {
      this.name = value.name;
      this.logo = value.logo;
    });
    this.leagueservice.getAllTeamByLeagueId( 16).subscribe(value =>{
      value.sort((a,b)=>a.position-b.position);
      value.forEach( val =>{
        if(val.position !== 0){this.teams.push(val)};
      })
    });
  }


}
