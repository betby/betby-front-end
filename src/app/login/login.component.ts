import {Component, Input, OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import {Router} from "@angular/router";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  popUpSuccess : boolean = false;
  popUpFailure : boolean = false;
  errorMessage !: string;
  constructor(private _auth: AuthService, private _router : Router) { }

  ngOnInit() {
  }

  loginUser(event: any) {
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;

    this._auth.loginUser({username, password}).subscribe({
      next: data => {
        localStorage.setItem('token',data.token);
        localStorage.setItem('userName', data.userName);
        localStorage.setItem('id', data.id);
        localStorage.setItem('role',data.role);
        localStorage.setItem('currencyAmount',data.currencyAmount);
        localStorage.setItem('avatarColor',data.avatarColor);
        localStorage.setItem('avatarLabel',data.avatarLabel);
        this.popUpSuccess = true;
      },
      error: error => {
        this.errorMessage = error.error;
        this.popUpFailure = true;
      }
    });
    }
    onClickToConnect(){
      this._router.navigateByUrl('profil').then();
    }
    onClickToLanding(){
      this._router.navigateByUrl('').then();
    }
    onClickToRetry(){
      this.popUpFailure = false;
  }
}


