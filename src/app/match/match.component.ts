import {Component, Input, OnInit} from '@angular/core';
import {Match} from "../models/match.model";
import {BetService} from "../services/bet.service";
import {AuthService} from "../services/auth.service";
import {Bet} from "../models/bet.model";
import {formatDate} from "@angular/common";
import {Router} from "@angular/router";



@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit{

  @Input() match!: Match;
  lastbetclick : string = "";
  userId : number = -1;
  isStarted! : boolean;
  message ! : string;
  doBet !: boolean;

  constructor(private betservice : BetService, private authservice : AuthService, private route : Router){}
  ngOnInit(): void {
    const userid = localStorage.getItem('id');
    if(userid){
      this.userId = Number.parseInt(userid);
    }
    this.isStarted = (formatDate(this.match.startDate, 'yyyy-MM-dd-HH-mm', 'en_US') < formatDate(new Date(), 'yyyy-MM-dd-HH-mm', 'en_US'));
    this.doBet = false;
  }
  onClickChoiceBet( choice : string){
    if(choice === this.lastbetclick){
      this.lastbetclick = "";
    }else{this.lastbetclick = choice;}
  }

  onClickbet( ){
    let moneybet;
    const input = document.getElementById(this.match.id.toString()) as HTMLInputElement | null;
    if(input){ //récupere la valeur parié
      moneybet = Number.parseInt(input.value);
    }else{return;}

    const teamidbet = (this.lastbetclick === 'oddslocal') ? "Home" : (this.lastbetclick === 'oddsdraw') ? "Draw" : (this.lastbetclick === 'oddsvisitor') ? "Visitor" : "";
    const bet = new Bet(this.userId, moneybet, this.match.id, teamidbet);

    this.betservice.addBetOnDB( bet ).subscribe( value =>{
      (value.betAccepted)? this.message = "Pari accepté !" : this.message = "Pari refusé !";
      this.doBet = true;
      this.authservice.getCurrencyAmount().subscribe(newCurrency =>{
        localStorage.setItem("currencyAmount",newCurrency);
      });
    });
  }
  onPopUpBetClick(){
    this.doBet  = false;
    this.route.navigateByUrl("mesparis").then();
  }
}
