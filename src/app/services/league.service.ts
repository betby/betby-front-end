import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Team} from "../models/team.model";
import {League} from "../models/league.model";
import {VariablesGlobales} from "../variablesGlobale";

@Injectable({
  providedIn: 'root'
})
export class LeagueService {

  private readonly url : string;
  constructor(private http: HttpClient, private _server : VariablesGlobales) {
    this.url = _server.server;
  }

  getLeagueById( id : number) : Observable<League>{
    return this.http.get<League>(this.url+`/League/GetLeague/${id}`);
  }

  getAllTeamByLeagueId(id : number) : Observable<Team[]>{
    return this.http.get<Team[]>(this.url+`/Team/GetAllTeamsByLeagueId/${id}`);
  }


}
