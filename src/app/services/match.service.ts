import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Match} from "../models/match.model";
import {VariablesGlobales} from "../variablesGlobale";

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  private readonly url : string;
  constructor(private http: HttpClient, private _server : VariablesGlobales) {
    this.url = _server.server;
  }

  getAllMatchs() : Observable<Match[]>{
    return this.http.get<Match[]>(this.url+"/Match/GetAllMatches");
  }
  getAllMatchesByTeamId(id : number) : Observable<Match[]> {
    return this.http.get<Match[]>(this.url+`/Match/GetAllMatchesByTeamId/${id}`);
  }
  getMatch(id: number) : Observable<Match>{
    return this.http.get<Match>(this.url + `/Match/GetMatch/${id}`);
  }
  getPastMatches(idleague : number) : Observable<Match[]>{
    return this.http.get<Match[]>(this.url+`/Match/GetPastMatches/${idleague}`);
  }

  getCurrentMatches(idleague : number) : Observable<Match[]>{
    return this.http.get<Match[]>(this.url+`/Match/GetCurrentMatches/${idleague}`);
  }
  getFutureMatches(idleague : number) : Observable<Match[]>{
    return this.http.get<Match[]>(this.url+`/Match/GetFutureMatches/${idleague}`);
  }


}
