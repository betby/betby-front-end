import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {VariablesGlobales} from "../variablesGlobale";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly _loginUrl : string;
  private readonly _registerUrl: string;
  private readonly _currencyUrl : string;

  private readonly _deleteAccount : string;

  constructor(private http: HttpClient, private _router: Router, _server: VariablesGlobales) {
    this._loginUrl = _server.server + "/Auth/login";
    this._registerUrl = _server.server + "/Auth/register";
    this._currencyUrl = _server.server + "/Auth/currency/";
    this._deleteAccount = _server.server + "/Auth/user/";

  }

  loginUser(user: any) {
    return this.http.post<any>(this._loginUrl, user);
  }

  registerUser(user: any) {
    return this.http.post<any>(this._registerUrl, user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  logoutUser() {
    localStorage.clear();
    this._router.navigateByUrl('/login').then();
  }

  getToken() {
    return localStorage.getItem('token');
  }
  getCurrencyAmount(): Observable<string>{
    let userid = localStorage.getItem('id');
    return this.http.get<string>(this._currencyUrl + userid );
  }
  deleteUser() {
    let userid = localStorage.getItem('id');
    return this.http.delete(this._deleteAccount + userid);
  }
}
