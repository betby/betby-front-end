import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {VariablesGlobales} from "../variablesGlobale";
import {Observable} from "rxjs";
import {Bet} from "../models/bet.model";
import {BetResult} from "../models/bet-result.model";

@Injectable({
  providedIn: 'root'
})
export class BetService {

  private readonly url : string;
  constructor(private http: HttpClient, private _server : VariablesGlobales) {
    this.url = _server.server;
  }

  getAllBets() : Observable<BetResult[]>{
    return this.http.get<BetResult[]>(this.url+`/Bet/GetAllBets`);
  }

  getAllBetsByUserId( id : number) : Observable<BetResult[]>{
    return this.http.get<BetResult[]>(this.url+`/Bet/GetAllBetsByUserId/${id}`);
  }

  getBetById( id : number) : Observable<BetResult>{
    return this.http.get<BetResult>(this.url+`/Bet/GetBetById/${id}`);
  }

  addBetOnDB( bet : Bet) : Observable<BetResult>{
    return this.http.post<BetResult>(this.url+`/Bet/AddBet`, bet);
  }

  deleteBet( id : number){
    return this.http.delete(this.url+`/Bet/DeleteBet/${id}`);
  }
}
