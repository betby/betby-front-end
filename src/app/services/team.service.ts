import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Team} from "../models/team.model";
import {VariablesGlobales} from "../variablesGlobale";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private readonly url :string ;
  constructor(private http: HttpClient, private _server : VariablesGlobales) {
    this.url = _server.server;
  }

  getTeamById(id : number, idleague : number) : Observable<Team>{
    return this.http.get<Team>(this.url +`/Team/GetTeam/${id}/${idleague}`);
  }



}
