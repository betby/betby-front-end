import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes} from "@angular/router";
import {RouterModule} from "@angular/router";
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {StandingsComponent} from "./standings/standings.component";
import {MatchListComponent} from "./match-list/match-list.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {ProfileComponent} from "./profile/profile.component";
import {LoggedGuard} from "./logged.guard";
import {MatchListByTeamComponent} from "./match-list-by-team/match-list-by-team.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {BetListComponent} from "./bet-list/bet-list.component";

const routes : Routes = [
  {path : 'classement', component : StandingsComponent},
  {path : 'allMatchs', component : MatchListComponent},
  {path : '', component : LandingPageComponent},
  {path : 'login', component : LoginComponent},
  {path : 'register', component : RegisterComponent},
  {path : 'profil', component : ProfileComponent, canActivate:[LoggedGuard]},
  {path : 'matchsByTeam/:id', component : MatchListByTeamComponent},
  {path : 'mesparis', component: BetListComponent, canActivate:[LoggedGuard]},
  {path : '**', component :NotFoundComponent}
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
